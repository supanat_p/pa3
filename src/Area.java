
/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Enumeration of area's unit
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public enum Area implements Unit {
	
	SQUAREMETER( "squareMeter" , 1.0 ),
	SQUAREKILOMETER( "squareKilometer" , 1000000.0 ),
	SQUARECENTIMETER( "squareCentimeter" , 0.0001 ),
	HECTARE( "hectare" , 10000.0 ),
	ACRE( "acre" , 4046.8564224 ),
	SQUAREMILE( "squareMile" , 2589990.0 ),
	SQUAREFOOT( "squareFoot" , 0.09290304 ),
	RAI( "rai" , 1600.0 ),
	SQUAREWA( "squareWa" , 4.0 );
	
	/** Name of this unit. */
	private String name;
	/** Value of this unit. */
	private double value;
	
	/**
	 * Constructor of area's unit.
	 * @param name is a name of this unit
	 * @param value is a amount of this unit
	 */
	Area( String name , double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert the value to another unit.
	 * @param unit is a converted unit
	 * @param amount is a value that converted
	 * @return value that converted
	 */
	public double convertTo( Unit unit , double amount ) {
		return amount * this.value / unit.getValue();
	}
	
	/**
	 * Get value of this unit.
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get a description.
	 * @return String of name of this unit
	 */
	public String toString() {
		return this.name;
	}

}
