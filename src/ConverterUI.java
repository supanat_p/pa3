/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * User Interface for unit's converter.
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public class ConverterUI extends JFrame {
	
	/** Declare the attribute. */
	private JMenuBar mb_menuBar;
	private UnitConverter unitConverter;
	private JMenu menu;
	private JMenuItem length , area , weight , volume;
	private JComboBox cb_fromUnit , cb_toUnit;
	private JLabel lb_equal;
	private JTextField txt_fromUnit , txt_toUnit;
	private JButton btn_convert , btn_clear;
	
	/** String that keep recent value that converted. */
	private String temp1 , temp2;
	
	/**
	 * Constructor of the class.
	 * @param unitConverter is an unitConverter to convert the unit
	 */
	public ConverterUI( UnitConverter unitConverter) {
		this.unitConverter = unitConverter;
		initComponents();
	}
	
	/**
	 * Initialize the interface of the program.
	 */
	public void initComponents() {
		
		/** Close the program when click the cross. */
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/** Set the title of the program. */
		setTitle( "Unit Converter" );
		
		/** Initialize the String to null. */
		temp1 = "";
		temp2 = "";
		
		/** Initialize the menu's bar. */
		mb_menuBar = new JMenuBar();
		mb_menuBar.setBackground(Color.LIGHT_GRAY);
		super.setJMenuBar(mb_menuBar);
		menu = new JMenu("Unit Type");
		
		/** Initialize the label to " = " */
		lb_equal = new JLabel( " = " ); 
		
		/** Initialize the left text field to null. */
		txt_fromUnit = new JTextField(15);
		txt_fromUnit.setText("");
		
		/** Add an action to text field. */
		txt_fromUnit.addActionListener( new ActionListener() {
			
			/**
			 * If press Enter while cursor in text field,
			 * It will convert the value from left text field
			 * to the right unit in the right text field.
			 * @param e is an event
			 */
			public void actionPerformed(ActionEvent e) {
				try{
					double amount = Double.parseDouble(txt_fromUnit.getText());
					if( amount < 0 )
						throw new DataFormatException();
					Unit fromUnit = (Unit)cb_fromUnit.getSelectedItem();
					Unit toUnit = (Unit)cb_toUnit.getSelectedItem();
					txt_toUnit.setText(unitConverter.convert(amount, fromUnit, toUnit)+"" );
					temp1 = txt_fromUnit.getText();
					temp2 = txt_toUnit.getText();
				}
				
				/**
				 * If input number is not a number, it will throw.
				 * and change text to red color.
				 * @param ex is an exception
				 */
				catch( NumberFormatException ex ){
					txt_fromUnit.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Number");
					txt_fromUnit.setForeground(Color.black);
				}
				
				/**
				 * If input number is a negative number, it will throw.
				 * and change text to red color.
				 * @param ex is an exception
				 */
				catch( DataFormatException ex ){
					txt_fromUnit.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Positive Number");
					txt_fromUnit.setForeground(Color.black);
				}
			}
		});
		
		/** Initialize the right text field to null. */
		txt_toUnit = new JTextField(15);
		txt_toUnit.setText("");
		/** Add an action to right text field. */
		txt_toUnit.addActionListener( new ActionListener() {
			
			/**
			 * If press Enter while cursor in right text field,
			 * It will convert the value from right text field
			 * to the left's unit in the left text field.
			 * @param e is an event
			 */
			public void actionPerformed(ActionEvent e) {
				try{
					double amount = Double.parseDouble(txt_toUnit.getText());
					if( amount < 0 )
						throw new DataFormatException();
					Unit fromUnit = (Unit)cb_toUnit.getSelectedItem();
					Unit toUnit = (Unit)cb_fromUnit.getSelectedItem();
					txt_fromUnit.setText(unitConverter.convert(amount, toUnit, fromUnit)+"");
					temp1 = txt_fromUnit.getText();
					temp2 = txt_toUnit.getText();
				}
				
				/**
				 * If input number is not a number, it will throw.
				 * and change text to red color.
				 * @param ex is an exception
				 */
				catch( NumberFormatException ex ){
					txt_toUnit.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Number");
					txt_toUnit.setForeground(Color.black);
				}
				
				/**
				 * If input number is a negative number, it will throw.
				 * and change text to red color.
				 * @param ex is an exception
				 */
				catch( DataFormatException ex ){
					txt_toUnit.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Positive Number");
					txt_toUnit.setForeground(Color.black);
				}
			}
		});
		
		/** Initialize the COnvert button. */
		btn_convert = new JButton( "Convert !" );
		
		/** Add an action to the button. */
		btn_convert.addActionListener( new ActionListener() {
			
			/**
			 * If you change the value in left text field and click the button,
			 * It will change the value in the right text field.
			 * Same as when change the right text field.
			 * @param e is an event
			 */
			public void actionPerformed(ActionEvent e) {
				
				/**
				 * Check how to change in left text field.
				 * case 1 : first convert and input in right text field
				 * case 2 : left text field is blank and 
				 * input a number in right text field
				 * case 3 : Change the number in right text field
				 * and not change left text field
				 */
				if( ( temp1.equals("") && temp2.equals("") && txt_fromUnit.getText().equals("") ) ||
					( txt_fromUnit.getText().equals("") && !txt_toUnit.getText().equals("") ) ||
					(!txt_toUnit.getText().equals(temp2)) && !txt_toUnit.getText().equals("") ){
					try{
						double amount = Double.parseDouble(txt_toUnit.getText());
						if( amount < 0)
							throw new DataFormatException();
						Unit fromUnit = (Unit)cb_fromUnit.getSelectedItem();
						Unit toUnit = (Unit)cb_toUnit.getSelectedItem();
						txt_fromUnit.setText(unitConverter.convert(amount, toUnit, fromUnit)+"");
					}

					/**
					 * If input number is not a number, it will throw.
					 * and change text to red color.
					 * @param ex is an exception
					 */
					catch( NumberFormatException ex ){
						txt_toUnit.setForeground(Color.RED);
						JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Number");
						txt_toUnit.setForeground(Color.black);
					}
					
					/**
					 * If input number is a negative number, it will throw.
					 * and change text to red color.
					 * @param ex is an exception
					 */
					catch( DataFormatException ex ){
						txt_toUnit.setForeground(Color.RED);
						JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Positive Number");
						txt_toUnit.setForeground(Color.black);
					}
				}	
				else {	
					try{
						double amount = Double.parseDouble(txt_fromUnit.getText());
						if( amount < 0 )
							throw new DataFormatException();
						Unit fromUnit = (Unit)cb_fromUnit.getSelectedItem();
						Unit toUnit = (Unit)cb_toUnit.getSelectedItem();
						txt_toUnit.setText(unitConverter.convert(amount, fromUnit, toUnit)+"");
					}

					/**
					 * If input number is not a number, it will throw.
					 * and change text to red color.
					 * @param ex is an exception
					 */
					catch( NumberFormatException ex ){
						txt_fromUnit.setForeground(Color.RED);
						JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Number");
						txt_fromUnit.setForeground(Color.black);
					}

					/**
					 * If input number is a negative number, it will throw.
					 * and change text to red color.
					 * @param ex is an exception
					 */
					catch( DataFormatException ex ){
						txt_fromUnit.setForeground(Color.RED);
						JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Positive Number");
						txt_fromUnit.setForeground(Color.black);
					}
				}
				/** Set the value to each number in text field. */
				temp1 = txt_fromUnit.getText();
				temp2 = txt_toUnit.getText();
			}
		});
		
		/** Initialize the clear button. */
		btn_clear = new JButton( "Clear !");
		
		/** Add an action the the button. */
		btn_clear.addActionListener( new ActionListener() {
			
			/**
			 * Set the text field to blank.
			 * @param e is an event
			 */
			public void actionPerformed(ActionEvent e) {
				txt_fromUnit.setText("");
				txt_toUnit.setText("");
			}
		});
		
		/** Initialize the combo boxes. */
		Unit [] units = Length.values();
		cb_fromUnit = new JComboBox(units);
		cb_toUnit = new JComboBox(units);
		
		/** Add an item to the menu in menu bar. */
		mb_menuBar.add(menu);
		for(int i=0 ; i<unitConverter.getUnitTypes().length ; i++ ) {
			menu.add(new ConverterAction(unitConverter.getUnitTypes()[i]));
		}
		menu.add( new ExitAction() );
		
		/** Declare and initialize the container. */
		Container container = super.getContentPane();
		container.setLayout( new FlowLayout() );
		container.add(txt_fromUnit);
		container.add(cb_fromUnit);
		container.add(lb_equal);
		container.add(txt_toUnit);
		container.add(cb_toUnit);
		container.add(btn_convert);
		container.add(btn_clear);
		pack();
		super.setVisible(true);
	}
	
	/**
	 * Nested class for Initialize the class of each unit. 
	 * @author Supanat Pokturng
	 * @version 2015.03.08
	 */
	class ConverterAction extends AbstractAction {
		
		/** Type of the unit. */
		private UnitType unitType;
		
		/**
		 * Constructor of the class.
		 * @param unitType is a type of unit
		 */
		public ConverterAction(UnitType unitType){
			
			/** Set text to show in menu. */
			super(unitType.toString());
			this.unitType = unitType;
		}
		
		/**
		 * Clear the text and history when select new type.
		 * Show new combo box if select another unit
		 * @param e is an event
		 */
		public void actionPerformed( ActionEvent e ) {
			temp1 = "";
			temp2 = "";
			txt_fromUnit.setText("");
			txt_toUnit.setText("");
			cb_fromUnit.setModel( new DefaultComboBoxModel(unitType.getUnit()));
			cb_toUnit.setModel( new DefaultComboBoxModel( unitType.getUnit()));
			pack();
		}
	}
	
	/**
	 * Nested class for exit the program.
	 * @author Supanat Pokturng
	 * @version 2015.03.08
	 */
	class ExitAction extends AbstractAction {
		
		/**
		 * Constructor of the class.
		 */
		public ExitAction() {
			/** Show Exit's text in the menu. */
			super("Exit");
		}
		
		/**
		 * Exit the program when click.
		 * @param e is an event
		 */
		public void actionPerformed( ActionEvent e ) {
			System.exit(0);
		}
	}
}