
/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Enumeration of Volume's unit
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public enum Volume implements Unit {
	
	CUBICMETER( "cubicMeter" , 1.0 ),
	CUBICCENTIMETER( "cubicCentimeter" , 0.000001 ),
	CUBICDECIMETER( "cubicDecimeter" , 0.001 ),
	LITER( "liter" , 0.001 ),
	CUBICFOOT( "cubicFoot" , 0.028316847 ),
	CUBICINCH( "cubicInch" , 0.000016387 ),
	GWIAN( "gwian" , 2 );
	
	/** Name of this unit. */
	private String name;
	/** value of this unit. */
	private double value;
	
	/**
	 * Constructor of this unit.
	 * @param name is a name of this unit
	 * @param value is an amount of this unit
	 */
	Volume( String name , double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert the value to another unit.
	 * @param unit is a converted unit
	 * @param amount is a value that converted
	 * @return value that converted
	 */
	public double convertTo( Unit unit , double amount ) {
		return amount * this.value / unit.getValue();
	}
	
	/**
	 * Get value of this unit.
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get a description.
	 * @return String of name of this unit
	 */
	public String toString() {
		return this.name;
	}
}
