
/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Enumeration of unit's type.
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public enum UnitType {
	LENGTH {
		
		/**
		 * Get an array of length's unit.
		 * @return array of length's unit.
		 */
		public Unit[] getUnit() {
			return Length.values();
		}
	},
	AREA {
		
		/**
		 * Get an array of area's unit.
		 * @return array of area's unit
		 */
		public Unit[] getUnit() {
			return Area.values();
		}
	},
	WEIGHT {
		
		/**
		 * Get an array of weight's unit.
		 * @return array of weight's unit
		 */
		public Unit[] getUnit() {
			return Weight.values();
		}
	},
	VOLUME {
		
		/**
		 * Get an array of volume's unit.
		 * @return array of volume's unit
		 */
		public Unit[] getUnit() {
			return Volume.values();
		}
	};
	
	/**
	 * Debug the program.
	 * @return null
	 */
	public Unit[] getUnit() {
		return null;
	}
}
