
/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Converter of the units.
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public class UnitConverter {
	
	/**
	 * Convert the unit from one unit to another unit.
	 * @param amount is value that convert
	 * @param fromUnit is a unit's initial
	 * @param toUnit is a final unit
	 * @return the value of the converted unit.
	 */
	public double convert( double amount , Unit fromUnit , Unit toUnit ) {
		return fromUnit.convertTo(toUnit ,  amount );
	}
	
	/**
	 * Get an array of the unit of each type.
	 * @param unitType is a type of unit.
	 * @return array of units
	 */
	public Unit [] getUnits( UnitType unitType) {
		return unitType.getUnit();
	}
	
	/**
	 * Get an array of a type of units.
	 * @return array of a type of unit
	 */
	public UnitType [] getUnitTypes() {
		return UnitType.values();
	}
}
