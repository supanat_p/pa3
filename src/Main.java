
/**
 * This source code is Copyright 2015 by Supanat Pokturng
 * Application for run the program.
 * @author Supanat Pokturng
 *
 */
public class Main {
	
	/**
	 * Execute the program.
	 * @param args is an arguments
	 */
	public static void main( String [] args ) {
		
		/** Declare and initialize the UnitCoverter. */
		UnitConverter unitConverter = new UnitConverter();
		/** Sent the unitConverter to create the interface. */
		ConverterUI converterUI = new ConverterUI(unitConverter);
	}
}
