
/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Interface to keep every units.
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public interface Unit {
	
	/**
	 * Convert the unit from one unit to another unit.
	 * @param unit is the final unit
	 * @param amount is the value that convert
	 * @return value that converted
	 */
	public double convertTo( Unit unit , double amount);
	
	/**
	 * Get a value of the unit.
	 * @return value of unit.
	 */
	public double getValue();
	
	/**
	 * Get a description of the unit.
	 * @return String of a name of the unit
	 */
	public String toString();
}
