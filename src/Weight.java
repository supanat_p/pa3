
/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Enumeration of Weight's unit
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public enum Weight implements Unit {
	
	KILOGRAM( "kilogram" , 1.0 ),
	GRAM( "gram" , 0.001 ),
	MILLIGRAM( "milligram" , 0.000001 ),
	TON( "ton" , 1000.0 ),
	POUND( "pound" , 0.453592 ),
	OUNCE( "ounce" , 0.0283495 ),
	CARRAT( "carrat" , 0.0002 ),
	THANG( "thang" , 15.0 );
	
	/** Name of this unit. */
	private String name;
	/** Value of this unit. */
	private double value;
	
	/**
	 * Constructor of weight's unit.
	 * @param name is a name of this unit.
	 * @param value is an amount of this unit.
	 */
	Weight( String name , double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert the value to another unit.
	 * @param unit is a converted unit
	 * @param amount is a value that converted
	 * @return value that converted
	 */
	public double convertTo( Unit unit , double amount ) {
		return amount * this.value / unit.getValue();
	}
	
	/**
	 * Get value of this unit.
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get a description.
	 * @return String of name of this unit
	 */
	public String toString() {
		return this.name;
	}
}
