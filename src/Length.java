/**
 * This source code is copyright 2015 by Supanat Pokturng.
 * Enumeration of length's unit
 * @author Supanat Pokturng
 * @version 2015.03.08
 */
public enum Length implements Unit {
	METER( "meter" , 1.0 ),
	CENTIMETER( "centimeter" , 0.01 ),
	MILLIMETER( "millimeter" , 0.001),
	KILOMETER( "kilometer" , 1000.0 ),
	MILE( "mile" , 1609.344 ),
	YARD( "yard" , 0.9144 ),
	FOOT( "foot" , 0.30480 ),
	INCH( "inch" , 0.0254 ),
	WA( "wa" , 2.0 );
	
	
	/** Name of the unit. */
	private String name;
	/** Value of each unit. */
	private double value;
	
	/**
	 * Constructor of the enumeration.
	 * @param name is a name of unit
	 * @param value is value of the unit
	 */
	Length( String name , double value) {
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Convert the value to another unit.
	 * @param unit is a converted unit
	 * @param amount is a value that converted
	 * @return value that converted
	 */
	public double convertTo( Unit unit , double amount ) {
		return amount * this.value / unit.getValue();
	}
	
	/**
	 * Get value of this unit.
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get a description.
	 * @return String of name of this unit
	 */
	public String toString() {
		return this.name;
	}
}
